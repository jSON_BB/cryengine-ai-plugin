#pragma once
#include "Blackboard.h"

struct SNode
{
public:
	string name;
	virtual bool CheckConditions() { return false; }
	virtual void Start() {}
	virtual void End() {}
	virtual void DoAction() { LeafSucceeded(); }
	virtual void ChildFailed() {}
	virtual void ChildSucceeded() {}
	virtual void ChildInProcess() {}
	virtual void LeafFailed() {}
	virtual void LeafSucceeded() {}
	virtual void LeafInProcess() {}
	virtual void ReRun() {}
	virtual void SetUp(SNode *pNewParent, CBlackboard *pBB)
	{
		pParent = pNewParent;
		pBlackboard = pBB;
	}
	virtual SNode *AddChild(SNode *pNewChild)
	{
		pChild[iChildCount] = pNewChild;
		pChild[iChildCount]->SetParent(this);
		iChildCount += 1;
		return pNewChild;
	}
	virtual void SetParent(SNode *pNewParent)
	{
		pParent = pNewParent;
	}
	//Instead of iterating through all of them 
	//to save resources we will update count everytime new child is added
	virtual int GetChildCount()
	{
		return iChildCount;
	}
	virtual int GetLastChildIndex()
	{
		return iChildCount - 1;
	}
	bool returnValue = false;
protected:
	SNode *pChild[10] = { nullptr };
	int iCurChildIndex = 0;
	int iChildCount = 0;
	SNode *pParent = nullptr;
	CBlackboard *pBlackboard = nullptr;
};