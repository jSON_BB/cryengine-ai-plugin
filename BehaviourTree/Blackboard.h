#pragma once
#include "CryEntitySystem/IEntitySystem.h"

struct SNode;
struct IFlowGraphModule;
class CAIFuryComponent;
class CAITagPointComponent;

class CBlackboard
{
public:
	CBlackboard::CBlackboard(CAIFuryComponent *pai) { pAI = pai; }
	CAIFuryComponent *GetAIParent() { return pAI; }
	SNode *currentNode = nullptr;
	IFlowGraphModule *currentModule = nullptr;
	bool bIsWaiting = false;
private:
	CAIFuryComponent *pAI = nullptr;

	std::vector<std::pair<string, int> > ints;
	std::vector<std::pair<string, float> > floats;
	std::vector<std::pair<string, bool> > booleans;
	std::vector<std::pair<string, string> > strings;
	std::vector<std::pair<string, Vec3> > vectors;
	std::vector<std::pair<string, EntityId> > entityIds;

public:

	IEntity *pEnemyEntity = nullptr;
	IEntity *pLockedEnemy = nullptr;
	CAITagPointComponent *pClosestHidePoint = nullptr;
	bool bIsWalkingToClosestHidePoint = false;
	float fClosestHidePointDistance = 0.f;
	bool bIsWalkingAtAll = false;
	string selectorStr = "Selector";
	string sequenceStr = "Sequence";
	string leafStr = "Leaf";
	string treeLeafStr = "Tree";
	string inverterStr = "Inverter";
	string alwaysTrueStr = "AlwaysTrue";
	string alwaysFalseStr = "AlwaysFalse";
	Vec3 vHidePos = ZERO;
	Vec3 vLastSeenEnemyPosition = ZERO;

	void AddKey(string keyType, string keyName)
	{
		CryLogAlways("ADDING BLACKBOARD KEY: KEY TYPE = %s, KEY NAME = %s", keyType, keyName);
		if (keyType == "Int")
		{
			for (int i = 0; i < ints.size(); i++)
			{
				if (ints.at(i).first == keyName)
					return;
			}
			ints.push_back(std::pair<string, int>(keyName, 0));
		}
		else if (keyType == "Float")
		{
			for (int i = 0; i < floats.size(); i++)
			{
				if (floats.at(i).first == keyName)
					return;
			}
			floats.push_back(std::pair<string, float>(keyName, 0.f));
		}
		else if (keyType == "Bool")
		{
			for (int i = 0; i < booleans.size(); i++)
			{
				if (booleans.at(i).first == keyName)
					return;
			}
			booleans.push_back(std::pair<string, bool>(keyName, false));
		}
		else if (keyType == "String")
		{
			for (int i = 0; i < strings.size(); i++)
			{
				if (strings.at(i).first == keyName)
					return;
			}
			strings.push_back(std::pair<string, string>(keyName, ""));
		}
		else if (keyType == "Vector3")
		{
			for (int i = 0; i < vectors.size(); i++)
			{
				if (vectors.at(i).first == keyName)
					return;
			}
			vectors.push_back(std::pair<string, Vec3>(keyName, Vec3(0.f, 0.f, 0.f)));
		}
		else if (keyType == "EntityId")
		{
			for (int i = 0; i < entityIds.size(); i++)
			{
				if (entityIds.at(i).first == keyName)
					return;
			}
			entityIds.push_back(std::pair<string, EntityId>(keyName, 0));
		}
	}
	//
	std::pair<string, int> *FindKey_Int(string keyName)
	{
		for (int i = 0; i < ints.size(); i++)
		{
			if (ints.at(i).first == keyName)
				return &ints.at(i);
		}
		return nullptr;
	}
	//
	std::pair<string, float> *FindKey_Float(string keyName)
	{
		for (int i = 0; i < floats.size(); i++)
		{
			if (floats.at(i).first == keyName)
				return &floats.at(i);
		}
		return nullptr;
	}
	//
	std::pair<string, bool> *FindKey_Bool(string keyName)
	{
		for (int i = 0; i < booleans.size(); i++)
		{
			if (booleans.at(i).first == keyName)
				return &booleans.at(i);
		}
		return nullptr;
	}
	//
	std::pair<string, string> *FindKey_String(string keyName)
	{
		for (int i = 0; i < strings.size(); i++)
		{
			if (strings.at(i).first == keyName)
				return &strings.at(i);
		}
		return nullptr;
	}
	//
	std::pair<string, Vec3> *FindKey_Vec3(string keyName)
	{
		for (int i = 0; i < vectors.size(); i++)
		{
			if (vectors.at(i).first == keyName)
				return &vectors.at(i);
		}
		return nullptr;
	}
	//
	std::pair<string, EntityId> *FindKey_EntityId(string keyName)
	{
		for (int i = 0; i < entityIds.size(); i++)
		{
			if (entityIds.at(i).first == keyName)
				return &entityIds.at(i);
		}
		return nullptr;
	}
	//
	EntityId GetKeyValue_EntityId(string keyName)
	{
		for (int i = 0; i < entityIds.size(); i++)
		{
			if (entityIds.at(i).first == keyName)
				return entityIds.at(i).second;
		}
		return 0;
	}
};