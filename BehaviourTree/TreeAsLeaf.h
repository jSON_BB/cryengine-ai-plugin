#pragma once
#include "Node.h"

class CTreeNode : public SNode
{
public:
	CTreeNode::CTreeNode(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CTreeNode::CTreeNode(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CTreeNode::CTreeNode(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
	virtual void ChildInProcess() override;
};