#include "Leaf.h"

void CLeaf::Start()
{
	if (CheckConditions())
	{
		DoAction();
	}
	else
	{
		LeafFailed();
	}
}

void CLeaf::LeafFailed()
{
	if (pParent)
	{
		End();
		pParent->ChildFailed();
	}
	else
	{
	}
}

void CLeaf::LeafSucceeded()
{
	if (pParent)
	{
		End();
		pParent->ChildSucceeded();
	}
	else
	{
	}
}

void CLeaf::LeafInProcess()
{
	if (pParent)
	{
		pParent->ChildInProcess();
	}
	else
	{
		DoAction();
	}
}

bool CLeaf::CheckConditions()
{
	return true;
}
