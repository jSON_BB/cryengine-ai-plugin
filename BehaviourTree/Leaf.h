#pragma once
#include "Node.h"

class CLeaf : public SNode
{
public:
	CLeaf::CLeaf(){}
	CLeaf::CLeaf(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CLeaf::CLeaf(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	virtual void Start() override;
	virtual void LeafFailed() override;
	virtual void LeafSucceeded() override;
	virtual void LeafInProcess() override;
	virtual bool CheckConditions();
};
