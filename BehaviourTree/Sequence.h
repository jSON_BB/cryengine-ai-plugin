#pragma once
#include "Node.h"

class CSequence : public SNode
{
public:
	CSequence::CSequence(CBlackboard *pBB)
	{
		pBlackboard = pBB;
	}
	CSequence::CSequence(SNode *pParentTask, CBlackboard *pBB)
	{
		pParent = pParentTask;
		pBlackboard = pBB;
	}
	CSequence::CSequence(SNode *pParentTask = nullptr, CBlackboard *pBB = nullptr, XmlNodeRef pNode = nullptr);
	virtual void Start() override;
	virtual void ChildFailed() override;
	virtual void ChildSucceeded() override;
	virtual void ChildInProcess() override;
};