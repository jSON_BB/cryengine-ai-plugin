#include "StdAfx.h"
#include "Plugin.h"

#include <CrySchematyc/Env/IEnvRegistry.h>
#include <CrySchematyc/Env/EnvPackage.h>
#include <CrySchematyc/Utils/SharedString.h>


// Included only once per DLL module.
#include <CryCore/Platform/platform_impl.inl>

#include "BehaviourTree/AIFuryComponent.h"

#include <atlbase.h>
#include <sstream>

#include <FlashUI/FlashUI.h>
#include <FlashUI/FlashUIAction.h>

int CAIFury::pTagPointsCount = 0;
CAITagPointComponent *CAIFury::pTagPointsList[10000] = { nullptr };
bool CAIFury::ALLOW_PLUGIN = false;

void CAIFury::OnUIEventEx(IUIElement * pSender, const char * fscommand, const SUIArguments & args, void * pUserData)
{
	string eventName = fscommand;
	const string _auth = "AUTHORIZED";

	//if (ALLOW_PLUGIN)
	{
		if (eventName == _auth)
		{
			ALLOW_PLUGIN = true;
			if (Element) 
				Element->RemoveEventListener(this);
		}
		else
		{
			ALLOW_PLUGIN = false;
			if (Element)
				Element->RemoveEventListener(this);
		}
	}
}

CAIFury::~CAIFury()
{
	gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);

	if (gEnv->pSchematyc)
	{
		gEnv->pSchematyc->GetEnvRegistry().DeregisterPackage(CAIFury::GetCID());
	}
	UnregisterFlowNodes();
}

CAIFury::CAIFury()
{
#if DEBUG
	//
	ALLOW_PLUGIN = true;
//#else
//	const string group = ".DEFAULT\\Software\\ztx16_crt_6f\\pth";
//	const string key = "fr_crt";
//	char value[255];
//	DWORD bufferSize = 8192;
//
//	RegGetValue(HKEY_USERS, group, key, RRF_RT_ANY, NULL, (PVOID)&value, &bufferSize);
//	string keyValue = value;
//
//	keyValue.Delete(keyValue.length() - 28, 28);
//
//	char path[MAX_PATH + 1];
//	HMODULE hm = NULL;
//
//	if (!GetModuleHandleExA(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT, "AIFuryPlugin.dll", &hm))
//	{
//	}
//	GetModuleFileNameA(hm, path, sizeof(path));
//	string modulePath = path;
//
//	int count = 29;
//	modulePath.Delete(modulePath.length() - count, count);
//
//	if (keyValue == modulePath)
//	{
//		ALLOW_PLUGIN = true;
//	}
#endif

	//ALLOW_PLUGIN = true;
	RegisterFlowNodes();
}

bool CAIFury::RegisterFlowNodes()
{
	CryRegisterFlowNodes();
	return true;
}

bool CAIFury::UnregisterFlowNodes()
{
	CryUnregisterFlowNodes();
	return true;
}

bool CAIFury::Initialize(SSystemGlobalEnvironment& env, const SSystemInitParams& initParams)
{
	gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this,"CAIFury");
	SetUpdateFlags(EUpdateType_Update);
	CryLogAlways("AIFury loaded succesfuly.");

	//if (ALLOW_PLUGIN)
		CreateUIFiles();
	return true;
}

void CAIFury::OnPluginUpdate(EPluginUpdateType updateType)
{
}

void CAIFury::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
	case ESYSTEM_EVENT_REGISTER_SCHEMATYC_ENV:
	{
		// Register all components that belong to this plug-in
		auto staticAutoRegisterLambda = [](Schematyc::IEnvRegistrar& registrar)
		{
			// Call all static callback registered with the CRY_STATIC_AUTO_REGISTER_WITH_PARAM
			Detail::CStaticAutoRegistrar<Schematyc::IEnvRegistrar&>::InvokeStaticCallbacks(registrar);
		};

		if (gEnv->pSchematyc)
		{
			gEnv->pSchematyc->GetEnvRegistry().RegisterPackage(
				stl::make_unique<Schematyc::CEnvPackage>(
					CAIFury::GetCID(),
					"EntityComponents",
					"Crytek GmbH",
					"Components",
					staticAutoRegisterLambda
					)
			);
		}
	}
	break;
	case ESYSTEM_EVENT_LEVEL_GAMEPLAY_START:
	{
#if DEBUG
#else	
		//if (ALLOW_PLUGIN)
			InitializeUI();
#endif
	}
	break;
	}
}

void CAIFury::InitializeUI()
{
	if (!gEnv->pFlashUI)
	{
		return;
	}
	if (Element = gEnv->pFlashUI->GetUIElement("AIFury"))
	{
		Element->AddEventListener(this, "AIFury");
		if (IUIActionManager *pMan = gEnv->pFlashUI->GetUIActionManager())
		{
			if (IUIAction *pAction = gEnv->pFlashUI->GetUIAction("aifuryshow"))
			{
				if (XmlNodeRef actionDoc = gEnv->pSystem->LoadXmlFromFile("Libs/UI/UIActions/aifuryshow.xml"))
				{
					pMan->StartAction(pAction, "AIFury");
					return;
				}
			}
		}
	}
	//ALLOW_PLUGIN = false;
}

void CAIFury::CreateUIFiles()
{
	//CREATE ACTION
	int id[5] = { 2, 3, 5, 6, 7 };
	string nodeClass[5] = { "UI:Action:Start", "UI:Action:End", "UI:Display:Config", "UI:Display:Constraints", "UI:Display:Display" };
	string pos[5] = { "-666,-360,0","306,-180,0","-234,-306,0","-414,72,0","-648,-252,0" };
	string inputAtt[5][2];
	inputAtt[0][0] = "UseAsState";
	inputAtt[0][1] = "1";

	inputAtt[1][0] = "UseAsState";
	inputAtt[1][1] = "1";

	inputAtt[2][0] = "uiElements_Element";
	inputAtt[2][1] = "AIFury";

	inputAtt[3][0] = "uiElements_Element";
	inputAtt[3][1] = "AIFury";

	inputAtt[4][0] = "uiElements_Element";
	inputAtt[4][1] = "AIFury";

	XmlNodeRef graphNode = gEnv->pSystem->CreateXmlNode("Graph");
	graphNode->setAttr("Description", "");
	graphNode->setAttr("Group", "");
	graphNode->setAttr("enabled", "1");
	graphNode->setAttr("MultiPlayer", "ClientServer");
	XmlNodeRef nodesNode = gEnv->pSystem->CreateXmlNode("Nodes");
	graphNode->addChild(nodesNode);
	for (int i = 0; i < 5; i++)
	{
		XmlNodeRef nodeNode = gEnv->pSystem->CreateXmlNode("Node");
		nodeNode->setAttr("Id", id[i]);
		nodeNode->setAttr("Class", nodeClass[i]);
		nodeNode->setAttr("pos", pos[i]);
		XmlNodeRef inputsNode = gEnv->pSystem->CreateXmlNode("Inputs");
		nodesNode->addChild(nodeNode);
		inputsNode->setAttr(inputAtt[i][0], inputAtt[i][1]);

		if (i == 1)
			inputsNode->setAttr("Args", "");

		if (i == 2)
		{
			inputsNode->setAttr("instanceID", "-1");
			inputsNode->setAttr("cursor", "0");
			inputsNode->setAttr("mouseEvents", "0");
			inputsNode->setAttr("keyEvents", "0");
			inputsNode->setAttr("consoleMouse", "0");
			inputsNode->setAttr("consoleCursor", "0");
			inputsNode->setAttr("controllerInput", "0");
			inputsNode->setAttr("eventsExclusive", "0");
			inputsNode->setAttr("fixedProjDepth", "0");
			inputsNode->setAttr("forceNoUnload", "0");
			inputsNode->setAttr("alpha", "0");
			inputsNode->setAttr("layer", "0");
		}

		if (i == 3)
		{
			inputsNode->setAttr("instanceID", "-1");
			inputsNode->setAttr("type", "2");
			inputsNode->setAttr("left", "0");
			inputsNode->setAttr("top", "0");
			inputsNode->setAttr("width", "480");
			inputsNode->setAttr("height", "135");
			inputsNode->setAttr("scale", "0");
			inputsNode->setAttr("hAlign", "0");
			inputsNode->setAttr("vAlign", "2");
			inputsNode->setAttr("maximize", "0");
		}

		if (i == 4)
			inputsNode->setAttr("instanceID", "-1");

		nodeNode->addChild(inputsNode);
	}

	XmlNodeRef edgesNode = gEnv->pSystem->CreateXmlNode("Edges");

	string nodeIn[4] = { "7", "6", "3", "5" };
	string nodeOut[4] = { "2", "5", "6", "7" };
	string portIn[4] = { "show", "set", "EndAction", "set" };
	string portOut[4] = { "StartAction", "OnSet", "OnSet", "onShow" };
	string enabled[4] = { "1", "1", "1", "1" };

	for (int i = 0; i < 4; i++)
	{
		XmlNodeRef edgeNode = gEnv->pSystem->CreateXmlNode("Edge");
		edgeNode->setAttr("nodeIn", nodeIn[i]);
		edgeNode->setAttr("nodeOut", nodeOut[i]);
		edgeNode->setAttr("portIn", portIn[i]);
		edgeNode->setAttr("portOut", portOut[i]);
		edgeNode->setAttr("enabled", enabled[i]);
		edgesNode->addChild(edgeNode);
	}

	graphNode->addChild(edgesNode);
	graphNode->addChild(gEnv->pSystem->CreateXmlNode("GraphTokens"));
	graphNode->saveToFile("Libs/UI/UIActions/aifuryshow.xml");
	//~CREATE ACTION
	//CREATE ELEMENT
	XmlNodeRef uiElementsNode = gEnv->pSystem->CreateXmlNode("UIElements");
	{
		uiElementsNode->setAttr("name", "AIFury");
		XmlNodeRef uiElementNode = gEnv->pSystem->CreateXmlNode("UIElement");
		{
			uiElementNode->setAttr("name", "AIFury");
			uiElementNode->setAttr("render_lockless", "1");
			XmlNodeRef gfxNode = gEnv->pSystem->CreateXmlNode("GFx");
			{
				gfxNode->setAttr("file", "AIFury.gfx");
				gfxNode->setAttr("layer", "0");
				XmlNodeRef consNode = gEnv->pSystem->CreateXmlNode("Constraints");
				{
					XmlNodeRef alignNode = gEnv->pSystem->CreateXmlNode("Align");
					alignNode->setAttr("mode", "dynamic");
					alignNode->setAttr("valign", "center");
					alignNode->setAttr("halign", "center");
					alignNode->setAttr("scale", "1");
					alignNode->setAttr("max", "1");
					consNode->addChild(alignNode);
					gfxNode->addChild(consNode);
					uiElementNode->addChild(gfxNode);
				}
			}
			XmlNodeRef fNode = gEnv->pSystem->CreateXmlNode("functions");
			XmlNodeRef eNode = gEnv->pSystem->CreateXmlNode("events");
			XmlNodeRef vNode = gEnv->pSystem->CreateXmlNode("variables");
			XmlNodeRef ANode = gEnv->pSystem->CreateXmlNode("Arrays");
			XmlNodeRef MNode = gEnv->pSystem->CreateXmlNode("MovieClips");
			uiElementNode->addChild(fNode);
			uiElementNode->addChild(eNode);
			uiElementNode->addChild(vNode);
			uiElementNode->addChild(ANode);
			uiElementNode->addChild(MNode);

			uiElementsNode->addChild(uiElementNode);
		}
	}

	uiElementsNode->saveToFile("Libs/UI/UIElements/AIFury.xml");
	//~CREATE ELEMENT
}

CRYREGISTER_SINGLETON_CLASS(CAIFury)