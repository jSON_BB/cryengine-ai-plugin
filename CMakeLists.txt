cmake_minimum_required (VERSION 3.6.2)
set(CRYENGINE_DIR "E:/Cryengine 5/CRYENGINE Launcher/Crytek/CRYENGINE_5.4")
set(TOOLS_CMAKE_DIR "${CRYENGINE_DIR}/Tools/CMake")

set(PROJECT_BUILD 1)
set(PROJECT_DIR "E:/Cryengine 5/CRYENGINE Launcher/Crytek/Projects/AIFury/AIFury v0.2/Plugin")

include("${TOOLS_CMAKE_DIR}/CommonOptions.cmake")

add_subdirectory("${CRYENGINE_DIR}" "${CMAKE_CURRENT_BINARY_DIR}/CRYENGINE")

include("${TOOLS_CMAKE_DIR}/Configure.cmake")
start_sources()

sources_platform(ALL)
add_sources("Code_uber.cpp"
    PROJECTS Plugin
    SOURCE_GROUP "Root"
		"FlowNodes.cpp"
		"Plugin.cpp"
		"StdAfx.cpp"
		"FlowNodes.h"
		"FuryResources.h"
		"Plugin.h"
		"StdAfx.h"
)
add_sources("BehaviourTree_uber.cpp"
    PROJECTS Plugin
    SOURCE_GROUP "BehaviourTree"
		"BehaviourTree/AIFuryComponent.cpp"
		"BehaviourTree/Inverter.cpp"
		"BehaviourTree/AlwaysTrue.cpp"
		"BehaviourTree/AlwaysFalse.cpp"
		"BehaviourTree/Leaf.cpp"
		"BehaviourTree/LeafFlowgraph.cpp"
		"BehaviourTree/Selector.cpp"
		"BehaviourTree/Sequence.cpp"
		"BehaviourTree/TreeAsLeaf.cpp"
		"BehaviourTree/AIFuryComponentProperties.h"
		"BehaviourTree/AIFuryComponent.h"
		"BehaviourTree/Blackboard.h"
		"BehaviourTree/Inverter.h"
		"BehaviourTree/AlwaysTrue.h"
		"BehaviourTree/AlwaysFalse.h"
		"BehaviourTree/Leaf.h"
		"BehaviourTree/LeafFlowgraph.h"
		"BehaviourTree/Node.h"
		"BehaviourTree/Selector.h"
		"BehaviourTree/Sequence.h"
		"BehaviourTree/TreeAsLeaf.h"
)

end_sources()

CryEngineModule(Plugin PCH "StdAfx.cpp" SOLUTION_FOLDER "Project")

target_include_directories(${THIS_PROJECT}
PRIVATE 
    "${CRYENGINE_DIR}/Code/CryEngine/CryCommon"
    "${CRYENGINE_DIR}/Code/CryEngine/CryAction"
	"${CRYENGINE_DIR}/Code/CryEngine/CrySchematyc/Core/Interface"
	"${CRYENGINE_DIR}/Code/CryPlugins/CryDefaultEntities/Module"
)

# Set StartUp project in Visual Studio

add_library(GameLauncher STATIC "${CRYENGINE_DIR}/Code/CryEngine/CryCommon/CryCore/Platform/platform.h")
set_target_properties(GameLauncher PROPERTIES LINKER_LANGUAGE CXX)
if (WIN32)
    set_visual_studio_debugger_command(GameLauncher "${CRYENGINE_DIR}/bin/win_x64/GameLauncher.exe" "-project \"E:/Cryengine 5/CRYENGINE Launcher/Crytek/Projects/AIFury/AIFury v0.2/Plugin/Plugin.cryproject\"")
endif()

add_library(Sandbox STATIC "${CRYENGINE_DIR}/Code/CryEngine/CryCommon/CryCore/Platform/platform.h")
set_target_properties(Sandbox PROPERTIES LINKER_LANGUAGE CXX)
if (WIN32)
    set_visual_studio_debugger_command(Sandbox "${CRYENGINE_DIR}/bin/win_x64/Sandbox.exe" "-project \"E:/Cryengine 5/CRYENGINE Launcher/Crytek/Projects/AIFury/AIFury v0.2/Plugin/Plugin.cryproject\"")
endif()

add_library(GameServer STATIC "${CRYENGINE_DIR}/Code/CryEngine/CryCommon/CryCore/Platform/platform.h")
set_target_properties(GameServer PROPERTIES LINKER_LANGUAGE CXX)
if (WIN32)
    set_visual_studio_debugger_command(GameServer "${CRYENGINE_DIR}/bin/win_x64/Game_Server.exe" "-project \"E:/Cryengine 5/CRYENGINE Launcher/Crytek/Projects/AIFury/AIFury v0.2/Plugin/Plugin.cryproject\"")
endif()

set_solution_startup_target(GameLauncher)

if (WIN32)
    set_visual_studio_debugger_command( ${THIS_PROJECT} "${CRYENGINE_DIR}/bin/win_x64/GameLauncher.exe" "-project \"E:/Cryengine 5/CRYENGINE Launcher/Crytek/Projects/AIFury/AIFury v0.2/Plugin/Plugin.cryproject\"" )
endif()

#BEGIN-CUSTOM
# Make any custom changes here, modifications outside of the block will be discarded on regeneration.
#END-CUSTOM